#!/usr/bin/env python3

import requests
from net.elmau.zaz.EasyMacro import (XTools, XArrays, XDict, XEmail,
    XRequests, Response)
import easymacro as app


_dicts = {}


class Tools(XTools):
    value = None
    OS = app.OS
    USER = app.USER
    PC = app.PC
    DESKTOP = app.DESKTOP
    LANGUAGE = app.LANGUAGE
    LANG = app.LANG
    NAME = app.NAME
    VERSION = app.VERSION
    INFO_DEBUG = app.INFO_DEBUG
    IS_WIN = app.IS_WIN

    def __init__(self):
        pass

    def get_size_screen(self):
        return app.get_size_screen()

    def question(self, message, title):
        if not title:
            title = app.TITLE
        return app.question(message, title)

    def warning(self, message, title):
        if not title:
            title = app.TITLE
        return app.warning(message, title)

    def errorbox(self, message, title):
        if not title:
            title = app.TITLE
        return app.errorbox(message, title)

    def create_instance(self, name, with_context):
        return app.create_instance(name, with_context)

    def get_dispatch(self):
        return app.get_dispatch()

    def call_dispatch(self, url, args):
        return app.call_dispatch(url, args)

    def set_config(self, key, value, prefix):
        return app.set_config(key, value, prefix)

    def get_config(self, key, default, prefix):
        return app.get_config(key, default, prefix)

    def exists_app(self, name):
        return app.exists_app(name)

    def run(self, command, wait):
        return app.run(command, wait)

    def exists_path(self, path):
        return app.exists_path(path)

    def join(self, paths):
        return app.join(*paths)

    def get_config_path(self, name):
        if not name:
            name = 'Work'
        return app.get_config_path(name)

    def get_temp_file(self):
        f = app.get_temp_file()
        f.close()
        return f.name

    def get_file(self, init_dir, multiple, filters):
        files = app.get_file(init_dir, multiple, filters)
        if isinstance(files, list):
            files = tuple(files)
        return files

    def get_path(self, init_dir, filters):
        return app.get_path(init_dir, filters)

    def get_dir(self, init_dir):
        return app.get_dir(init_dir)

    def get_info_path(self, path):
        return app.get_info_path(path)

    def replace_ext(self, path, extension):
        return app.replace_ext(path, extension)

    def get_path_content(self, path, filters):
        paths = app.get_path_content(path, filters)
        return tuple(paths)

    def is_dir(self, path):
        return app.is_dir(path)

    def is_file(self, path):
        return app.is_file(path)

    def get_file_size(self, path):
        return app.get_file_size(path)

    def is_created(self, path):
        return app.is_created(path)

    def open_file(self, path):
        return app.open_file(path)

    def read_file(self, path, mode, array):
        return app.read_file(path, mode, array)

    def save_file(self, path, mode, data):
        return app.save_file(path, mode, data)

    def to_json(self, path, data):
        return app.to_json(path, data)

    def from_json(self, path):
        return app.from_json(path)

    def json_dumps(self, data):
        return app.json_dumps(data)

    def json_loads(self, data):
        return app.json_loads(data)

    def file_copy(self, source, target, name):
        return app.file_copy(source, target, name)

    def kill(self, path):
        return app.kill(path)

    def zip_content(self, path):
        return app.zip_content(path)

    def zip(self, source, target, mode, pwd):
        return app.zip(source, target, mode, pwd)

    def unzip(self, source, path, members, pwd):
        if not members:
            members = None
        if not pwd:
            pwd = None
        return app.unzip(source, path, members, pwd)

    def merge_zip(self, target, zips):
        return app.merge_zip(target, zips)

    def get_clipboard(self):
        return app.get_clipboard()

    def set_clipboard(self, value):
        return app.set_clipboard(value)

    def sleep(self, seconds):
        return app.sleep(seconds)

    def get_epoch(self):
        return app.get_epoch()

    def render(self, template, data):
        data = app.array_to_dict(data)
        return app.render(template, data)

    def format(self, template, data):
        return app.format(template, data)

    def call_macro(self, macro):
        data = {
            'library': macro.Library or 'Standard',
            'module': macro.Module,
            'name': macro.Name,
            'language': macro.Language or 'Python',
            'location': macro.Location or 'user',
            'thread': macro.Thread,
            'args': macro.Arguments or (),
        }
        return app.call_macro(data)

    def timer(self, name, seconds, macro):
        data = {
            'library': macro.Library or 'Standard',
            'module': macro.Module,
            'name': macro.Name,
            'language': macro.Language or 'Python',
            'location': macro.Location or 'user',
            'args': macro.Arguments or (),
        }
        return app.timer(name, seconds, data)

    def stop_timer(self, name):
        return app.stop_timer(name)

    def encrypt(self, data, password):
        return app.encrypt(data, password)

    def decrypt(self, token, password):
        return app.decrypt(token, password)

    def start(self):
        return app.start()

    def end(self):
        return app.end()

    def inputbox(self, message, default, title, echochar):
        if not title:
            title = app.TITLE
        return app.inputbox(message, default, title, echochar)

    def get_color(self, value):
        return app.get_color(value)

    def copy(self):
        return app.copy()


class Arrays(XArrays):

    def append(self, array, value):
        l = list(array)
        l.append(value)
        return tuple(l)

    def extend(self, array1, array2):
        l = list(array1)
        l.extend(list(array2))
        return tuple(l)

    def insert(self, array, index, value):
        l = list(array)
        l.insert(index, value)
        return tuple(l)

    def remove(self, array, value):
        l = list(array)
        l.remove(value)
        return tuple(l)

    def pop(self, array, index):
        l = list(array)
        value = l.pop(index)
        return (tuple(l), value)

    def clear(self, array):
        return ()

    def exists(self, array, value):
        return value in array

    def index(self, array, value):
        l = list(array)
        return l.index(value)

    def count(self, array, value):
        l = list(array)
        return l.count(value)

    def len(self, array):
        return len(array)

    def sort(self, array, column, reverse):
        l = list(array)
        if column > 0:
            l = sorted(l, key=lambda x:x[column], reverse=reverse)
        else:
            l.sort(reverse=reverse)
        return tuple(l)

    def reverse(self, array):
        l = list(array)
        l.reverse()
        return tuple(l)

    def acopy(self, array):
        l = list(array)
        return tuple(l.copy())

    def slice(self, array, value):
        if value.startswith('[') and value.endswith(']'):
            t = eval('{}{}'.format(array, value))
        return t

    def delete(self, array, index):
        l = list(array)
        if isinstance(index, str):
            if index.startswith('[') and index.endswith(']'):
                exec('del l{}'.format(index))
            else:
                l.remove(index)
        else:
            del l[index]
        return tuple(l)

    def unique(self, array):
        l = set(array)
        return tuple(l)

    def transpose(self, array):
        l = zip(*array)
        return tuple(l)

    def sum(self, array):
        return sum(array)

    def multi(self, array, value):
        return array * value

    def min(self, array):
        return min(array)

    def max(self, array):
        return max(array)

    def average(self, array):
        result = sum(array) / float(len(array))
        return result

    def equal(self, array1, array2):
        return array1 == array2

    def get_column(self, array, column):
        l = [row[column] for row in array]
        return tuple(l)

    def arange(self, index):
        l = eval('range({})'.format(index))
        return tuple(l)

    def intersection(self, array1, array2):
        s1 = set(array1)
        s2 = set(array2)
        s = s1.intersection(s2)
        return tuple(s)

    def union(self, array1, array2):
        s1 = set(array1)
        s2 = set(array2)
        s = s1.union(s2)
        return tuple(s)

    def difference(self, array1, array2):
        s1 = set(array1)
        s2 = set(array2)
        s = s1.difference(s2)
        return tuple(s)

    def symmetric_difference(self, array1, array2):
        s1 = set(array1)
        s2 = set(array2)
        s = s1.symmetric_difference(s2)
        return tuple(s)


class Dict(XDict):

    def init_dict(self, name, array):
        global _dicts
        _dicts[name] = app.array_to_dict(array)
        return

    def set_dict(self, name, key, value):
        global _dicts
        _dicts[name][key] = value
        return

    def get_dict(self, name, key=''):
        global _dicts
        if key:
            values = _dicts[name][key]
        else:
            values = tuple([(k, v) for k, v in _dicts[name].items()])
        return values

    def del_dict(self, name, key=''):
        if key:
            del _dicts[name][key]
        else:
            del _dicts[name]
        return

    def exists_dict(self, name, key=''):
        if key:
            result = key in _dicts[name]
        else:
            result = name in _dicts
        return result

    def get_keys(self, name):
        return tuple(_dicts[name].keys())

    def get_values(self, name):
        return tuple(_dicts[name].values())

    def update_dict(self, name, array):
        _dicts[name].update(app.array_to_dict(array))
        return


class AppEmail(XEmail):

    def server_smtp_test(self, server):
        config = app.array_to_dict(server)
        return app.server_smtp_test(config)

    def _message_to_dict(self, message):
        d = {
            'to': message.To,
            'cc': message.Cc,
            'bcc': message.Bcc,
            'subject': message.Subject,
            'body': message.Body,
            'files': message.Files,
            'path': message.Path,
        }
        return d

    def send_email(self, server, message):
        if isinstance(message, tuple):
            message = [self._message_to_dict(m) for m in message]
        else:
            message = self._message_to_dict(message)
        server = app.array_to_dict(server)
        return app.send_email(server, message)


class Requests(XRequests):

    def request(self, data):
        method = data.Method.upper() or 'GET'
        url = data.Url

        req = requests.Request(method, url)
        r = req.prepare()
        ses = requests.Session()
        res = ses.send(r)

        try:
            json = app.json_dumps(res.json())
        except ValueError:
            json = ''

        response = Response()
        response.StatusCode = res.status_code
        response.Encoding = res.encoding
        response.Url = res.url
        response.Text = res.text
        response.Json = json

        return response
